# -*- coding: utf-8 -*-
#-------------------------------
# Name:        drag_drop.py
# Author:      Tom
# Created:     06.07.2013
# Copyright:   (c) Snippsat 2013
#-------------------------------
import wx
import urllib2
import re
import subprocess
import threading
import random
from urllib import urlretrieve
from sub_text import xml2srt

def nrk(url, vid_quality, prog_name):
    '''Parse data for nrk-tv website'''
    quality = {'low' : 'index_2',
               'med' : 'index_3',
               'high': 'index_4'}
    url = urllib2.urlopen(url)
    url_1 = url.read()
    link = re.search(r'data-media="(.*)manifest', url_1)
    try:
        link = link.group(1)
        link = re.sub(r'/\w/', '/i/', link)
        link_add = quality[vid_quality]+\
        '_av.m3u8?null=#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=2390000,RESOLUTION=1280x720'
        fin_link = '{}{}'.format(link, link_add)
    except AttributeError:
        return False

    #--| Subtitle
    sub_pattern = re.search(r'data-subtitlesurl = "/.*/(.*)"', url_1)
    try:
        sub_name = sub_pattern.group(1)
    except AttributeError:
        sub_name = ''
    url_sub = 'http://tv.nrk.no/programsubtitles/'
    sub_name_full = '{}.xml'.format(sub_name)
    urlretrieve(url_sub+sub_name, sub_name_full)
    xml2srt(sub_name_full, prog_name)

    #--| cmd commands
    process = subprocess.Popen('cmd.exe /k ',\
    shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=None)
    process.stdin.write("ffmpeg -i {} -vcodec copy -acodec ac3 {}\n".format(fin_link, '{}.mkv'.format(prog_name)))
    o,e=process.communicate()
    process.stdin.close()

class MyURLDropTarget(wx.PyDropTarget):
    '''Drag and drop'''
    def __init__(self, window,radio_choice):
        wx.PyDropTarget.__init__(self)
        self.window = window
        self.vid_choice = radio_choice
        self.data = wx.URLDataObject();
        self.SetDataObject(self.data)

    def OnDragOver(self, x, y, d):
        return wx.DragLink

    def OnData(self, x, y, d):
        if not self.GetData():
            return wx.DragNone
        url = self.data.GetURL()
        url = re.sub(r'^tv', 'http://tv', url) #fix http
        #--| Parse program name
        try:
            url_tmp = urllib2.urlopen(url).read()
            p_name = re.search(ur'<title>NRK TV - (.*)<|NRK Super TV - (.*)</title>', url_tmp)
            if p_name.group(1) is None:
                tmp_1 = p_name.group(2)
            else:
                tmp_1 = p_name.group(1)
        except (AttributeError, urllib2.HTTPError):
            return False
        #--| Clean up prog_name,for use as filename & srt name
        tmp_2 = re.sub(r'\:\s', '_', tmp_1)
        tmp_3 = tmp_2.replace(':', '_')
        tmp_4 = re.sub(r'\s\-\s', '-', tmp_3)
        tmp_5 = re.sub(r'\s', r'_', tmp_4)
        tmp_6 = re.sub(ur'&#248;|&#216;', 'o', tmp_5)
        tmp_7 = re.sub(ur'&#229;|&#197;', 'aa', tmp_6)
        tmp_8 = re.sub(ur'&#230;|&#198;', 'ae', tmp_7)
        tmp_9 = re.sub(ur'&#214;|&#246;', 'o', tmp_8)
        tmp_10 = re.sub(ur'&quot;', '', tmp_9)
        tmp_11 = re.sub(r'\?', r'', tmp_10)
        prog_name = re.sub(ur'&amp;', 'og', tmp_11)
        #--| Run
        vid_quality = self.vid_choice
        #--| Allow multiple downlods
        t = threading.Thread(target=nrk, args=(url, vid_quality, prog_name,))
        t.start()
        self.window.AppendText(url + "\n")
        return d

class MyFrame(wx.Frame):
    '''Gui frontend'''
    def __init__(self, parent, mytitle, mysize):
        wx.Frame.__init__(self, parent, wx.ID_ANY, mytitle,\
        size=mysize,style=wx.DEFAULT_DIALOG_STYLE | wx.MINIMIZE_BOX)
        #--| Setup
        self.SetBackgroundColour('#bcb998')
        self.panel = wx.Panel(self)
        ico = wx.Icon('tv.ico', wx.BITMAP_TYPE_ICO)
        self.SetIcon(ico)
        #--| Picture
        wx.StaticBitmap(self.panel,-1,wx.Bitmap("nrk_pic.png",
              wx.BITMAP_TYPE_ANY),pos=(165,13), size=(200,24))
        #--| Button
        close = wx.Button(self.panel,-1,"Close all downloads",(388,23),(105,26))
        self.Bind(wx.EVT_BUTTON,self.EvtChoice, close)
        #--| Textbox
        self.dropText = wx.TextCtrl(self.panel, pos=(0,50), size=(495,212),
                          style=wx.TE_MULTILINE|wx.HSCROLL|wx.TE_READONLY)
        #--| Radiobuttons
        quality = ['low', 'med', 'high']
        rb = wx.RadioBox(self.panel,-1,"Video quality",(10,3),wx.DefaultSize,quality,3,wx.RA_SPECIFY_COLS)
        rb.SetSelection(2)
        self.Bind(wx.EVT_RADIOBOX, self.EvtRadioBox, rb)
        self.radio_choice = rb.GetStringSelection()
        #-- | Call dropText and set "high" vid_quality
        dt = MyURLDropTarget(self.dropText, self.radio_choice)
        self.dropText.SetDropTarget(dt)

    def EvtRadioBox(self, event):
        self.radio_choice = event.GetString()
        dt = MyURLDropTarget(self.dropText, self.radio_choice)
        self.dropText.SetDropTarget(dt)

    def EvtChoice(self, event):
        '''Shutdown ffmpeg process'''
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        subprocess.call('taskkill /F /IM ffmpeg.exe', startupinfo=startupinfo)

    def OnStartDrag(self, evt):
        if evt.Dragging():
            url = self.draggableURLText.GetValue()
            data = wx.URLDataObject()
            data.SetURL(url)
            dropSource = wx.DropSource(self.draggableURLText)
            dropSource.SetData(data)
            result = dropSource.DoDragDrop()

if __name__ == "__main__":
    app = wx.App()
    mytitle = 'Wx_nrk'
    width = 500
    height = 289
    MyFrame(None, mytitle, (width, height)).Show()
    app.MainLoop()


