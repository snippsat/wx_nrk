#-------------------------------
# Name:        sub_text.py
# Author:      Tom
# Created:     06.07.2013
# Copyright:   (c) Snippsat 2013
# Thx to gorlum0 for basic script
#-------------------------------
import sys
import os,re
import datetime as dt
import glob
from BeautifulSoup import BeautifulSoup

def xml2srt(f_name='', prog_name=''):
    with open(f_name) as fin:
        text = fin.read()

        text = text.replace('-1:', '00:')
        soup = BeautifulSoup(text)
    ps = soup.findAll('p')

    with open('%s.srt' % prog_name, 'w') as fout:
        try:
            os.remove(f_name) # Remove xml
        except(WindowsError, OSError):
            #print sys.exc_info()[1]
            pass
        for i, p in enumerate(ps,1):
            try:
                begin = p['begin']
                timestamp, seconds = begin[:5], begin[6:]
                begin = dt.datetime.strptime(timestamp, '%H:%M') + \
                        dt.timedelta(0, 0, float(seconds) * 1e6)
                dur = p['dur']
                timestamp, seconds = dur[:5], dur[6:]
                dur = dt.datetime.strptime(timestamp, '%H:%M') + \
                      dt.timedelta(0, 0, float(seconds) * 1e6)
                dur = dt.timedelta(0, dur.hour,
                        (dur.minute*60 + dur.second) * 1e6 + dur.microsecond)
                end = begin + dur
                begin = '%s,%03d' % (begin.strftime('%H:%M:%S'), begin.microsecond//1000)
                end   = '%s,%03d' % (end.strftime('%H:%M:%S'), end.microsecond//1000)
                #---| Clean text
                tag = str(p).replace('<br /> ', '')
                tag1 = str(tag).replace('<br />', '___')
                soup1 = BeautifulSoup(tag1)
                tmp = soup1.text.replace(ur'\u2014', '')
                #tmp_1 = tmp.replace('-', '') #Enable dash
                tmp_2 = re.sub(r'\.\b', '.\n', tmp)
                tmp_3 = re.sub(r'\. \b', '.\n', tmp_2)
                tmp_4 = re.sub(r'\?\b', '?\n', tmp_3)
                tmp_5 = re.sub(r'___', ' ', tmp_4) #Fix missing space
                clean = re.sub(r'\? \b', '?\n', tmp_5)
                #--- | Write subtext
                fout.write(str(i)+'\n')
                fout.write('%s --> %s\n' % (begin, end))
                fout.write(clean.encode('utf8')+'\n\n')
            #For now all error pass out,work on better error handling
            except:
                pass

if __name__ == '__main__':
    xml2srt(f_name)