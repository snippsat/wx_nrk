#nrk_minutt.py
'''
When NRK has a progam that take long time.
They spilt it up in diffrent parts,without changing the url adress
This is a example of download of differents part
'''
import urllib2
import re
import subprocess

def nrk(url, vid_quality,f_name):
    quality = {'low' : 'index_2',
               'med' : 'index_3',
               'high': 'index_4'}
    url = urllib2.urlopen(url).read()
    #link = re.search(r'data-media="(.*)manifest', url)
    link = re.search(r'data-argument="(.*)manifest.*(del 3:3)', url)
    link = link.group(1)
    link = re.sub(r'/\w/', '/i/', link)
    link_add = quality[vid_quality]+\
    '_av.m3u8?null=#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=2390000,RESOLUTION=1280x720'
    fin_link = '{}{}'.format(link, link_add)

    process = subprocess.Popen('cmd.exe /k ',\
    shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=None)
    process.stdin.write("ffmpeg -i {} -c copy {}\n".format(fin_link, '{}.mkv'.format(f_name)))
    o,e=process.communicate()
    process.stdin.close()

if __name__ == '__main__':
    url = 'http://tv.nrk.no/serie/sommeraapent/enrk22064313/02-08-2013'
    vid_quality = 'high'
    f_name = 'del_3'
    nrk(url, vid_quality, f_name)