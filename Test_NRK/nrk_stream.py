#nrk_stream.py
'''
Example of parse and download for NRK-TV
'''
import urllib2
import re
import subprocess

def nrk(url, vid_quality, f_name):
    quality = {'low' : 'index_2',
               'med' : 'index_3',
               'high': 'index_4'}
    url = urllib2.urlopen(url).read()
    link = re.search(r'data-media="(.*)manifest', url)
    link = link.group(1)
    link = re.sub(r'/\w/', '/i/', link)
    link_add = quality[vid_quality]+'_av.m3u8?null=#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=2390000,RESOLUTION=1280x720'
    fin_link = '{}{}'.format(link, link_add)
    #Run cmd commands
    process = subprocess.Popen('cmd.exe /k ', shell=True, stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=None)
    process.stdin.write("ffmpeg -i {} -c copy {}\n".format(fin_link, '{}.mkv'.format(f_name)))
    o,e=process.communicate()
    process.stdin.close()

if __name__ == '__main__':
    #---|Only make changes here|---#
    #Paste in url from nrk video you want to download
    url = 'http://tv.nrk.no/serie/livsfarlege-dyr/koid21001712/sesong-1/episode-12'
    #Video quality(low med high)
    vid_quality = 'high'
    #Name of mkv file
    f_name = 'livsfarlege-dyr'
    nrk(url, vid_quality, f_name)








